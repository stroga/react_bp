const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const devMode = process.env.NODE_ENV !== 'production';
module.exports = {
    entry: ['core-js/modules/es6.promise', 'core-js/modules/es6.array.iterator', './src/index.js'],
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                exclude: [/node_modules/],
                use: [
                    'css-hot-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [autoprefixer],
                        },
                    },
                    'sass-loader',
                ],
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader', 'eslint-loader'],
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.scss', '.json'],
    },
    output: {
        path: __dirname + '/browser',
        publicPath: '/',
        filename: 'bundle.js',
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new MiniCssExtractPlugin({
            filename: devMode ? '[name].css' : '[name].[hash].css',
            chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
        }),
        new webpack.NamedModulesPlugin(),
    ],
    devServer: {
        contentBase: './browser',
        hot: true,
        host: '0.0.0.0', // Docker fix https://github.com/microsoft/vscode-remote-release/issues/1009
    },
};
